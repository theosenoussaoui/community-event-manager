FROM composer:latest AS composer
COPY . /app
ARG ENVIRONMENT

RUN if [ "$ENVIRONMENT" = "dev" ]; then \
        set -xe && composer install --no-interaction; \
        composer dump-autoload; \
    elif [ "$ENVIRONMENT" = "prod" ]; then \
        set -xe && composer install --no-dev --no-scripts --no-suggest --no-interaction --optimize-autoloader; \
        rm -rf tests; \
        rm -rf config/routes/dev; \
        rm -rf config/packages/dev; \
        rm -rf config/packages/test; \
        composer dump-autoload --no-dev --optimize --classmap-authoritative; \
    fi

# -----------------------------------------

FROM node:11 AS node
COPY . app/
WORKDIR app/

RUN yarn install --check-files && yarn build;

# -----------------------------------------

FROM php:7.3-apache AS php

COPY . /var/www/symfony
WORKDIR /var/www/symfony

COPY . /app
COPY --from=composer /app/vendor /app/vendor
COPY --from=node /app/public /app/public

COPY ./docker/php-fpm/php-limit.ini /usr/local/etc/php/conf.d/php-limit.ini

ARG ENVIRONMENT

RUN if [ "$ENVIRONMENT" = "prod" ]; then \
        docker-php-ext-install opcache; \
        echo "opcache.memory_consumption=192" > /usr/local/etc/php/conf.d/opcache.ini && \
        echo "opcache.max_accelerated_files=16000" >> /usr/local/etc/php/conf.d/opcache.ini && \
        echo "opcache.validate_timestamps=0" >> /usr/local/etc/php/conf.d/opcache.ini && \
        echo "realpath_cache_size=4096K" >> /usr/local/etc/php/conf.d/opcache.ini && \
        echo "realpath_cache_ttl=600" >> /usr/local/etc/php/conf.d/opcache.ini && \
        echo "interned_strings_buffer=16" >> /usr/local/etc/php/conf.d/opcache.ini && \
        echo "fast_shutdown=1" >> /usr/local/etc/php/conf.d/opcache.ini; \
    elif [ "$ENVIRONMENT" = "dev" ]; then \
        pecl install xdebug-2.7.2 && \
        docker-php-ext-enable xdebug; \
    fi

#set our application folder as an environment variable
ENV APP_HOME /var/www/html
RUN sed -i -e "s/html/html\/public/g" /etc/apache2/sites-enabled/000-default.conf
RUN a2enmod rewrite

#copy source files and run composer
COPY . $APP_HOME

#change ownership of our applications
RUN chown -R www-data:www-data $APP_HOME